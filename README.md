# Automatic NPM Package CI+CD for GitLab

This provides a simple CI config to automatically release versions to NPM.

## Using

### Install

First you must configure the required credentials.

- `NPM_TOKEN`: A token to publish the crate.
- `SSH_PRIVATE_KEY`: A **file type** variable containing a SSH private key that has write access to the repository. Required to push tags.

Then simply create a `.gitlab-ci.yml` file containing the following:

```yaml
include:
- remote: 'https://gitlab.com/kevincox/node-ci/-/raw/v1/gitlab-ci.yml'
```

I also recommend protecting `v*` branches and tags. If you do this all of the secrets can be protected as well.

### Usage

This script will automatically release anything merged into a branch that looks like a version prefix. For example merging into `v1` will release a new minor version and merging into `v2.7` will create a patch release.

If you want to release on every push or merge simply use `v1` as your default branch. If you want to time your releases use a different branch (like `master` or `v1-next`) and merge into the release branch when you want to cut a release.

### Patches

The workflow set up here is aimed at making minor (and occasional major) releases. There intentionally isn't a trivial workflow for publishing patch releases. The idea is that patch releases are only used when you need a minor fixup and are an exceptional case. If you want to make a patch release you should manually create a `v{major}.{minor}` branch then merge into it. (Note that create a new branch from an existing release will not cause a new release.)
